import layoutparser as lp
from extract_layout import ExtractLayout
import pytesseract
from pdf_filter import pdf_filter
from tqdm import tqdm

pdf_path = "Paradoxes_of_Time_Travel.pdf"
config_path = "lp://PubLayNet/mask_rcnn_X_101_32x8d_FPN_3x/config"

model = ExtractLayout(
    config_path=config_path,
    label_map={0: "Text", 1: "Title", 2: "List", 3: "Table", 4: "Figure"},
    extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.75]
)

layouts, images = lp.io.load_pdf(pdf_path, dpi=300, load_images=True)

texts = []
for image in tqdm(images):
    texts.append(pdf_filter(pytesseract.image_to_string(image)))

layouts = []
for image in tqdm(images):
    layouts.append(model.detect(image))

# texts = [pdf_filter(pytesseract.image_to_string(image)) for image in images]
lines = [line for text in [text.split('\n\n') for text in texts] for line in text]

text_blocks = lp.Layout([b for b in layouts[2] if b.type=='Text'])
lp.draw_box(images[2], text_blocks, box_width=3).save('fig.png')
