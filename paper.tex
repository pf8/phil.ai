\documentclass[manuscript]{acmart}
\citestyle{acmauthoryear}
\setcopyright{none}
\usepackage{hyperref}

\title{Can Computers Do Philosophy?}

\author{Kevin Scharp}
\orcid{0000-0003-3900-4087}
\affiliation{
  \institution{University of Illinois}
  \city{Urbana-Champaign}
  \state{IL}
  \country{USA}
}
\email{kscharp@illinois.edu}
\author{Pyrros Rubani}
\affiliation{
  \institution{University of Illinois}
  \city{Urbana-Champaign}
  \state{IL}
  \country{USA}
}
\email{prubani2@illinois.edu}
\author{Hugh Sherriff}
\affiliation{
  \institution{University of Illinois}
  \city{Urbana-Champaign}
  \state{IL}
  \country{USA}
}
\email{ahs6@illinois.edu}
\author{Preston Firestone}
\affiliation{
  \institution{University of Illinois}
  \city{Urbana-Champaign}
  \state{IL}
  \country{USA}
}
\email{pf8@illinois.edu}

\begin{document}
\maketitle
\section{Introduction}\label{sec:introduction}
We want to program the computer to do philosophy. This begs the question: what is `doing philosophy'? This opens us to the objection that philosophizing is something that computers are unable to do. Perhaps the action of philosophizing is something that only human brains can do, so the closest one could get with the computer would be to simulate a brain that is doing philosophy. Or even more damning, perhaps philosophy is something that humans do in a community through their communication and collective cogitation: it's difficult to imagine how a computer could do such a thing, then, unless it perfectly simulated the actions of a whole community of people. It seems that, at least at the present state of knowledge, and perhaps in perpetuity, that the definitive answer to the question posed in this paper's title is no. In order to avoid answering this knock-down, drag-out objection, we shall make a number of simplifying assumptions, all of which can be understood as the strongest possible form of the ``death of the author'' thesis~\cite{barthes67_death_author}: there is no information about the process whereby the text was produced that has any pertinence in the intepretation of that text. This is an important point that is a necessary first step in turning the question we posed at the outset into a task that we can program that computer to do.

The first assumption we must make is that philosophy is encoded in strings, sequences of characters in a language. This can be understood as saying that the article itself is the philsophy. The alternative would be to say that the philosophy was done prior to the writing of the article, such that the article itself merely reported the philosophy that had already been done. This is how it is in experimental science: the part of the activity that is science properly speaking and which advances knowledge is the experiment; the article merely reports the results to other researchers. Similarly, philosophy could be a thing that happens in humans' heads, the results of which they report in articles. The quality of the philosophy done, then, is a question of the prior process that produced the paper, and not one about the paper itself. In theory, one could do a good job of philosophizing and still write a bad paper, or write a good paper reporting faulty philosophy, even as the quality of the paper produced about an experiment is not identical to the quality of the experiment done. We reject this, and say that the only quality that can possibly matter is the quality of the paper itself.

Philosophy, then, is made up of a set of strings: all the philosophical texts ever produced are a collective corpus that is philosophy. The process that generated these strings is insignificant. For example, one could suppose that Plato's dialogues were produced by some aleatory process (like the I Ching or the Tarot), and that would not have any impact on the philosophical quality of the text, assuming that the text were unchanged. If monkeys at typewriters eventually banged out the \textit{Parmenides}, that stochastically-generated dialogue would have exactly the same philosophical merit as the one that the person Plato wrote. It does not matter, for our purposes, whether or not the person inside of the Chinese Room, to steal an image from John Searle~\cite{searle_1980}, actually understands what they are generating: the philosophical quality of the result has nothing to do with who or what produced it and everything to do with the text itself.

The final assumption we must make is that the academic peer review system does an adequate job of filtering out papers that are not philosophically meritorious from those that are. This is not a very plausible assumption, but one it is necessary for two reasons. First, we need to assume that there already exist a body of texts that are the kinds of texts that we are looking to produce in order to avoid having to bootstrap ourselves into philosophy. Otherwise, we should first have to determine how to produce the texts at all, before we could make the computer do it. If we have target examples in hand, our task will be greatly facilitated. Secondly, we need an objective way to evaluate the success or failure of our project. The peer review system is, by our assumption, an adequate way to tell whether what we are trying to do worked.

From these assumptions, we can reformulate our problem as the following task, the solution of which will constitute an example of a computer doing something that is close enough to philsosophy to be worthy of serious consideration:
\begin{quotation}
Generate strings that will pass peer review at a philosophy journal.
\end{quotation}
In order to formalize the question, the rest of this section shall be devoted to introducing notational conventions used through the rest of the paper. First, let a string $s$ be a sequence of characters in a natural language. In this article we shall use the terms ``article'' or ``paper'' as synonyms for strings. The articles are assumed to have some sort of internal structure, but this structure is encoded directly in the string itself and can, for the remainder of this section, be safely ignored. We shall, in this section, treat the strings as opaque, before discussing their internal aspects in more detail in Section~\ref{sec:data}.

Let $\phi(s)$ be the ``philosophical quality'' of the string $s$. This value is latent and we cannot access it directly: philosophical quality is assumed to be the value that determines whether or not a paper is accepted. If we could directly program the computer to measure philosophical quality, then we could solve the problem by reinforcement learning to maximize $\phi$. Because this value is latent, we do not know its type: it might be that the quality of a string is best measured as a real number, perhaps as a vector, or as a tensor. Since we shall never access this quantity directly, we shall leave off specifying its type at this time.

As a proxy for $\phi(s)$, we have two alternatives. These are both easier to model since, as discussed in section \ref{sec:data}, a number of samples are available for them. The first of these is $o(s)$. $o$ is a function from a string to a string. The string that the function returns is an analysis of the quality of a string. It points out strengths, weaknesses, and other points of interest in the paper. The name $o$ is meant to evoke ``observation'' or ``oracle'': the call to $o$ is effectively the same as asking a peer to review the string.

The second function we have is called $r(s)$: the $r$ of a string $s$ is a real number in the range from 0 through 1 ($\mathbb{R} \in [0,1]$) and represents the probability that a string $s$ will be accepted at a journal. All published papers, for example, have an $r$ of 1; all rejected papers have an $r$ of 0. The $r$ of a paper that has not been submitted yet is a probability indicating the chances that the paper will be accepted. $r$ is meant to be a mnemonic for ``review, reward'' and is a proxy for the editor's final decision about an article. We shall call our generative process $\Gamma$ and refer to an ordinary background language model as $o$.

The rest of the paper is organized as follows: Section~\ref{sec:relatedwork} introduces related and similar work; Section~\ref{sec:data} discusses the available datasets in more depth; Section~\ref{sec:evaluation} discusses how we evaluate the success of our project; Section~\ref{sec:problemstatement} gives a formal statement of the model we shall use in terms introduced in Section~\ref{sec:relatedwork}; Section~\ref{sec:architecture} discusses how we implemented our model and Section~\ref{sec:results} the results of our trials. The last two sections,~\ref{sec:discussion} and~\ref{sec:conclusion}, respectively discuss the implications of the results and conclude the paper.

\section{Related Work}\label{sec:relatedwork}
\subsection{Formal Argumentation Frameworks}
Computer generated argumentation is indebted to Phan Minh Dung's seminal work on argumentation frameworks \cite{dung95, cerutti17_found_implem_formal_argum}. This strategy formalizes the intuition that ``the one who has the last word laughs best'' by framing a dispute as a set of arguments that attack one another. An argumentation framework can be represented as a directed graph where the nodes are the arguments and the edges are indicate that one argument attacks another. Dung gives us formal methods for determining which side is victorious. 

These methods of argumentation have a competition, the International Competition on Computational Models of Argumentation (ICCMA), focused on them~\cite{bistarelli24_third_fourt_inter_compet_comput_model_argum}. Libraries such as pygarg are available to process these problems, but they assume that the argument structure has already been abstracted into a graph representation~\cite{mailly24_pygar}. Our problem differs from this one in that we seek to manipulate natural language rather than abstract arguments. However, abstract argumentation may be useful for developing our enquiry, assuming we can formalize the informally stated argument.

An interesting direction forward, assuming that we accept this model of argumentation as being one that will produce good philosophy, would be to explore translating between arguments presented in natural language and formal argumentation frameworks. The subject continues to develop, for example incorporating estimates of the strength of arguments or making formal provisions for combining several weak arguments into a strong argument~\cite{beirlaen18_argum_stren_formal_argum, rossit21_united}. 

If one could identify, for each paper, what its arguments were and how they linked together, one could begin to make assessments of the quality of the paper. If one, further, were able to trace attacks between papers, one could prepare a graph representing the argumentation framework that philosophy as a whole makes up. This would make it possible to discover relations between arguments that have not yet been spotted.

\subsection{Reinforcement Learning Through Human Feedback}
Translating between natural language and formal argumentation frameworks might be made easier by a project like Beat AI. This web game, hosted by PhilPapers and run by David Bourget, aims to gather data to improve language models' abilities to understand philosophical terms.\footnote{https://philpeople.org/beatai} Bourget is using the data gathered to improve a model he has trained to embed sentences that use philosophical jargon to capture the similarity between the sentences.\footnote{https://huggingface.co/dbourget/philai-embeddings-v1.1}

\section{Data}\label{sec:data}
Two formalisms of section \ref{sec:introduction}, $r$ and $r$, lend themselves well to modeling. There are extensive data available to substantiate their distributions. As mentioned in the previous section, all published articles have an $r$ of 1, and all rejected articles have an $r$ of 0. Therefore an adequate dataset to describe $r$ would have individual articles as its data points (features) and values of 0 or 1 as its categories (labels). This makes the problem a simple binary classification problem, suitable for machine learning. See section \ref{sec:architecture} for a discussion of the use to which these data are be put.

$o$, though less simple than $r$ to model, nevertheless has a simple dataset to acquire: for each article, collect the set of peers' reviews. Each of these is a string of the required type. The features of the dataset are the submitted articles. Though this is not a classification problem and so is not as simple to model as $o$, it is still one that can be imitated by a computer. See section \ref{sec:evaluation} for a discussion of the use $o$ can be put to. 

\subsection{Corpus}
A feature of the domain we have not yet touched upon but which we know from our prior expertise will matter is that of cross referencing. The sorts of strings we model, academic articles, are distinguished from many other texts by their extensive use of citations and references. The computation that produces the values of $r$ and $o$ are assumed to have access to an entire corpus of articles, including but not limited to all those cited by the article they are examining. As a motivating example, suppose that an article fabricates references, or takes a quote out of context from another source: we expect that $o$ will call these errors out, and that they will reduce the ultimate value of $r$. The information necessary to produce these judgments must, at some point, be or have been accessible to the processes that produce the values of $r$ and $o$. See subsection \ref{sec:preprocessing} for more on citations in the text.

We also assume that we know which journals and publishers are the good ones. Determining what philosophy is or isn't is a contentious issue, and institutional authority is used to legitimize or deligitimize certain philosophic practices. For example, Deleuze-Guattari may or may not be considered philosophers of quality, depending on one's prior commitments. Though the framework set up in this paper proposes to be agnostic to the question of which papers are, in fact, of high philosophical quality, the corpora to which we have access will impose certain constraints upon the type of philosophy our generator will end up doing. 

The most likely source for papers is the PhilPapers archive.\footnote{https://philarchive.org/} This has already been partially processed for inclusion in The Pile dataset, but the preprocessing pipeline those authors used does not capture enough details of the structure of the text to be useful for our purposes: they remove headers and footers from the text of the PDFs by heuristics, but are not able to recover the position of the footnotes or their relationship to the main text \cite{gao20_pile}. Further, The Pile has been depricated and is no longer available for download: we could reproduce the dataset by running the scripts the original authors used, but the limitations of the dataset are discouraging, given our goals.

\subsection{Preprocessing}\label{sec:preprocessing}
The articles, as generally received from publishers, are actually images and not strings. They are stored in PDFs, files designed to describe the layout of text and images on a page. Humans apprehend most information visually, so laying the text out on a page allows the author to use the human visual perceptual system to their advantage. A number of structuring features are available visually. 

In order to accurately model the texts' features, we must be able to identify their structure. Identifying footnotes and their relationship to the text, citations and the article they reference, section and subsection structure, and so on. We shall also have to remove extraneous text (headers and footers, copyright notices, etc.). 

This problem can be solved both by extracting the strings from the PDF and then identifying the features; or by directly examining the image and identifying its parts, then extracting the text into a structured string. There has been work done in this field, but the current state of the art is not yet sufficient to entirely recover the structure of an article from its printed form~\cite{shen21_layout, boulanger22_extrac_excit, gemelli24_datas, pfitzmann22_doclay, wang21_layout}. For example, it is to our knowledge not yet possible to generally identify footnotes and determine where in the text they are referenced.

This problem would be entirely obviated by acquiring access to the original markup used to generate the PDFs (\LaTeX, XML, etc.). This, however, is a social more than technical problem, for more discussion of which see subsection \ref{sec:sourcing}.

\subsection{Sourcing}\label{sec:sourcing}
It is not obvious that the texts needed will be available: these texts are under copyright, and academic publishers are protective of this right. Similarly, since we need access to rejected papers as well as accepted ones, we shall require the collaboration of the publishers and spurned authors. 

Ideally, we should have access to the raw markup used to generate the PDFs we receive as a final product, as suggested in subsection \ref{sec:preprocessing}. However, this is a social issue that would require the collaboration of publishers and authors and one that could potentially increase the complexity of our task by introducing a greater variety of formats to our work.

\section{Evaluation}\label{sec:evaluation}
The ultimate standard for evaluation of our result, and the goal with which we enter into this project, is to submit a generated article to an actual journal and have it be actually accepted. If we can reliably generate papers that will actually be accepted, then we shall consider ourselves to have accomplished the goal set out at the beginning of section \ref{sec:introduction}. However, since the time of reviewers is expensive, we should not submit to a journal until we have a reasonably good chance of acceptance.

The intermediate evaluation strategy for our generative model, therefore, should be to maximize $r$. This goal is agnostic with regard to how it is solved: all we seek is some process $\Gamma$ that will generate strings $s$ whose $r$ is as close to 1 as possible. This $\Gamma$ will be our philosophizing computer.

The problem of evaluating our model for $r$ is much simpler: since this is a binary classification problem mapping strings to one of two categories, standard loss metrics are well suited to our goals. Even so is $o$ simple to evaluate: though it is more complicated to model than $r$, being a mapping from strings to strings rather than a binary classifier, we nevertheless expect that ordinary loss metrics that measure the divergence between the tokens generated by the model and those expected will be adequate for our needs. 

\section{Problem Statement}\label{sec:problemstatement}
This is a reinforcement learning problem: an agent acts in an environment to maximize a reward signal \cite{sutton18_reinf_learn}. There is an agent, call it $\Gamma$. This is our philosopher: it will generate strings for us. Assume that it can use a computer as well as any other philosopher, namely, as well as the authors, reviewers, and readers. It can take advantage of the same search engines, editors, and databases and we can. These advanced agential systems are proliferating \cite{wu23_autog, park23_gener_agent, mialon23_gaia, gao2024retrievalaugmented}, and we can assume that the system underlying the agent $\Gamma$ uses some or all of these techniques. Section \ref{sec:architecture} will discuss the implementation in more detail.

More importantly, we must specify the game the agent is playing. The agent's final output is a string: the action it takes is to produce a string. Each round, it makes an arbitrary number of revisions to a string. At the very simplest, every alteration to the string the model makes causes the environment to recompute $o$ and $r$. The agent's goal is to maximize the value of $r$. The game ends when the agent's string has an $r$ greater than some threshold set in advance.

Even more formally: the situation is a Markov decision process. The state is the string, the agent's actions are an edit plan to the string. The reward function is $r$, and the terminal state is when $r$ is above a certain value in a state.

That this is a \textit{Markov} decision process means that the entire dynamics of the agent-environment interaction be entirely determined by the immediately preceeding state and action: information about states and actions before the most recent ones are not available to the agent and are assumed to be unnecessary. This alone may disqualify our problem from being a Markov process and so complicate the reinforcement learning problem; but if it turns out to be necessary we can see to it that the state includes as much information about previous as is necessary, for example by presenting the agent with a history of its own actions or of the paper's versions.

The bigger problem is that this problem does not conform to what Sutton and Barto call the ``reward hypothesis'':
\begin{quote}
all of what we mean by goals and purposes can be well thought of as
the maximization of the expected value of the cumulative sum of a received scalar signal (called reward) \cite[][p. 53]{sutton18_reinf_learn}.
\end{quote}
In the formalism, the return $G_{t}$ is given by this sum \cite[][equation 3.8]{sutton18_reinf_learn}:
\begin{equation}
  G_{t} \doteq R_{t+1} + \gamma{}R_{t+2} + \gamma^{2}R_{t+3} + \dotsb = \sum^{\infty}_{k=0}\gamma^{k}R_{t+k+1}
\end{equation}
where $R_{t}$ is the reward at time $t$ and $\gamma$ is a ``discount rate'' in the range 0 through 1. The problem here is that the reward is \textit{cumulative}. This means that the agent will try to make it so that the sum of the successive $r$s is over time the most it can be. This is not the same as trying to get $r$ as high as possible, especially since we have proposed to stop the game after $r$ reaches a certain value on a single time step. What this means is that the agent might benefit more from turning in a mediocre paper many times than from turning in an excellent paper once. If the total reward gotten from the former action is greater than the latter, then the agent will be trained to prefer the former strategy.

The discount rate $\gamma$ will be useful to ensure that the agent has a reasonable pressure to get a high reward within a few turns. Setting the $\gamma$ to 0, for example, will make it so that the agent does not care about any reward except that available in the very next time step.  

\section{Architecture}\label{sec:architecture}
We have deferred discussion of architecture thus far, since in design it is important to clearly define the problem to be solved rather than immediately grasping on to a solution. We expect our attempt to solve this problem to be rudimentary at best and consider the primary contribution of this project to be a formalized account of the problem. Therefore we shall not seek novelty or complexity in our architecture: the purpose of the exercise is to show that our model of the problem is effective for structuring a solution, not to produce a general artificial intelligence.

We assume, at the outset, that we have a pre-trained language model, $o$, for the natural language in which our articles will be written (as it happens, English). This will come in handy for a number of processes and we expect to use $o$ in the implementation of $r$, $o$, and $\Gamma$. 

At its simplest, this can be understood as a reinforcement learning problem to tune the language model to a human preference, in this case to the preferences of journal editors~\cite{ziegler20_fine_tunin_languag_model_human_prefer, kaufmann24_survey_reinf_learn_human_feedb}. $\Gamma$ is an agent, $r$ is a reward model (to be maximized), and $o$ is an oracle the model can use to help it. 

We should note that an implementation of $r$ is assumed to have access to an implementation of $o$ as part of its process, though it may transpire that this is not necessary.

\section{Results}\label{sec:results}
\section{Discussion}\label{sec:discussion}
\section{Conclusion}\label{sec:conclusion}

\bibliographystyle{ACM-Reference-Format}
\bibliography{bibliography.bib}
\end{document}
